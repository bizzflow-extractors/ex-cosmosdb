# Azure Cosmos DB Extrator

* **autor:** Kate Strnkova (katarina.strnkova@bizztreat.com)
* **created:** 22.05.2020

## Description
Extractor for Azure Cosmos DB databases. 
##### What it is used for?
Extracting data from any Azure Cosmos DB database to the a specific folder on the local machine.
##### Why did you create it?
Because we need for out BI projects in Bizztreat.
##### What the code does?
The code - especially main.py file does:
1. Load the config file, where are all neccessary information for the extractor.
2. Create the connection between the machine, where is the script running and the remote database.
3. Ask for the specific data and save them to a specific folder on the local machine as CSV (everything is specified in the config.json file - read on).

## Requirements
* python3.5 or higher 
* [azure-cosmos](https://pypi.org/project/azure-cosmos/)


## How to use it?

#### Local use
1) Create or modify the config.json file:
	* config.json =  configuration:
    	- config file path: /config/config.json
        - check that the file is the corrent folder, if not create it
    	- before running the script fill the config.json file:
        	- all fields are required
        	- field "query" options: should contain valid sql selects
        * Example of the config.json
        ```json
          {
          "endpoint": "https://",
          "primarykey": "12345",
          "debug": true,
          "database": "logistics",
          "containers":[
               {
                  "name":"vehicleEvents",
                  "query":"SELECT c.column1, c.columns2 FROM c WHERE c.eventTime >= '{{export_from}}' AND c.eventTime < '{{export_to}}'",
                  "columns":[
                     "column1",
                     "columns2"
                  ]
               },
               {
                  "name":"vehicleLogs",
                  "query":"SELECT c.column1, c.columns2 FROM c WHERE c.eventTime >= '{{export_from}}' AND c.eventTime < '{{export_to}}'",
                  "columns":[
                     "column1",
                     "columns2"
                  ]
               }
            ],
          "export_from":"2020-05-19T00:00:00Z",
          "export_to":"2020-05-20T00:00:00Z",
          "block_count":"10000"
          }
        ```
            
2) Run the /scr/main.py
      ```sh
      python3 main.py
      ```
      