import azure.cosmos
import time
import os
import sys
import datetime
import json
import csv
import logging 
from argparse import ArgumentParser
import azure.cosmos.cosmos_client as cosmos_client
import azure.cosmos.errors as errors
import azure.cosmos.documents as documents

if not os.path.exists("/config/config.json"):
    raise Exception("Configuration not specified. Please check the path.")

with open("/config/config.json") as conf_file:
    conf = json.loads(conf_file.read())

if not os.path.exists("/data/out/tables"):
    os.makedirs("/data/out/tables")

## Argument Parser
args_parser = ArgumentParser(sys.argv[0])
args_parser.add_argument("--debug", action="store_true", default=False)
args = args_parser.parse_args(sys.argv[1:])

logging.basicConfig(format="[{asctime}] {levelname}: {message}", style="{", level=logging.INFO if conf["debug"]==False else logging.DEBUG)
logger = logging.getLogger(__name__)
    
## Create the Cosmos client
logger.debug("Establishing Cosmos client...")

client = cosmos_client.CosmosClient(url_connection=conf["endpoint"], auth={"masterKey":conf["primarykey"]})
logger.info("Cosmos client created.")

for i in range(len(conf["containers"])):
    logger.info("-"*40)
    logger.info("Processing '" + conf["containers"][i]["name"] + "' container.")
    
    ## Container Connection link
    logger.debug("Establishing connection link...")

    database_id = conf["database"]
    container_id = conf["containers"][i]["name"]
    collection_link = "dbs/" + database_id + "/colls/" + container_id

    logger.debug("Connection link created.")
    logger.debug("-"*40)    

    ## Limited timestamps
    export_from = conf["export_from"]
    export_to = conf["export_to"]
    
    ## Query
    query = conf["containers"][i]["query"].replace("{{export_from}}", export_from).replace("{{export_to}}", export_to)
    options = {'maxItemCount': conf["block_count"], 'continuation': True, 'enableCrossPartitionQuery': True}

    ## Queries documents in a collection
    logger.debug("Trying to query documents from CosmosDB in a collection.")
    items = client.QueryItems(collection_link, query, options)

    logger.debug("Running query")
    logger.debug("-"*40)
    logger.debug("Query : " + '"' + query + '"')

    logger.debug("Success!")
            
    ## Function for appending a list to csv
    def append_records(file_name, records, create_file = False):
        
        # Open file in write / append mode
        fnames = conf["containers"][i]["columns"]
        #mode = 'w' if create_file else 'a+'
        with open(file_name, 'w' if create_file else 'a+', newline='') as write_obj:
            # Create a writer object from csv module
            writer = csv.DictWriter(write_obj, fieldnames=fnames, dialect=csv.unix_dialect)
            if create_file:
                writer.writeheader()
                
            # Add contents of list as last row in the csv file
            for record in records:
                writer.writerow(record)
            
    current_lines = 0
    starting_csv = True
    
    ## Saves a collection into a list
    item_list = list(items.fetch_next_block())
    logger.debug("Saved the first block of " + conf["block_count"] + " rows from the collection into a list.")
    
    logger.debug("Starting with lists appending to csv files.")

    ## Appends a list to csv
    while (len(item_list) > 0):
        output_name = "/data/out/tables/{}.csv".format(conf["containers"][i]["name"])
        if starting_csv:
            append_records(output_name, item_list, True)
        else:
            append_records(output_name, item_list)
        starting_csv = False
        current_lines += len(item_list)
        logger.info(str(current_lines) + ' rows appended to ' + output_name)

        item_list = list(items.fetch_next_block())

    logger.info("Container '" + conf["containers"][i]["name"] + "' processed successfully. CSV file '" + conf["containers"][i]["name"] + ".csv' created.")