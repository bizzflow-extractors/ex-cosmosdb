FROM python:3.7-alpine

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="Bizzflow"
LABEL com.bizztreat.component="ex-cosmosdb"
LABEL com.bizztreat.title="CosmosDB Extractor"

RUN python -m pip install --upgrade pip setuptools wheel

ADD requirements.txt /

RUN python -m pip install -r /requirements.txt

VOLUME /data/out/tables
VOLUME /config

ADD src/ /code

WORKDIR /code

# Edit this any way you like it
ENTRYPOINT ["python", "-u", "main.py"]
